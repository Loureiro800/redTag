/* _optimizely_evaluate=force */
(function () {
    "use strict";
  
    window.redTag = {
        hideElement: function(element, hideElement){
            var D = document,
                selector;

            if(element !== undefined){
                if(element.indexOf('.') > -1){
                    selector = element.split('.');
                    selector = D.getElementsByClassName(selector[1])[0];
                } else {
                    selector = element.split('#');
                    selector = D.getElementById(selector[1]);
                }

                if(hideElement){            
                    if (D.body === null) {
                        setTimeout(function() { window.redTag.hideElement(element, true); }, 10);
                    } else {
                        if (selector !== undefined && selector !== null) {
                            selector.style.visibility = "hidden";
                        } else {
                            setTimeout(function() { window.redTag.hideElement(element, true); }, 10);
                        }
                    }
                } else {
                    if (selector !== undefined && selector !== null) {
                        selector.style.visibility = "inherit";
                    }
                }
            }        
        },
        loadingBar: function(activeElement) {
            var D = document;
            if(activeElement){
                var styles = '<style>#loading-redEye-bg{background: #fff; position: fixed; top:0; left: 0; width: 100%; height: 100%; z-index: 999999;}#loading-redEye:after { border-width: 0 3px 0 0; border-style: solid; border-color: rgba(0, 0, 0, .5); border-radius: 50%; display: block; height: 50px; left: 50%; margin: -25px 0 0 -25px; position: absolute; z-index: 999998; top: 50%; width: 50px; content: ""; animation: spin 1s infinite linear; -webkit-animation: spin 1s infinite linear; } @keyframes spin { from { transform: rotate(0deg); } to { transform: rotate(360deg); } } @-webkit-keyframes spin { from { -webkit-transform: rotate(0deg); } to { -webkit-transform: rotate(360deg); } }</style>';
                var first = D.body.children[0];
                var beforeEle = D.createElement("div");
                beforeEle.innerHTML = styles+'<div id="loading-redEye-bg"><div id="loading-redEye" /></div>';
                D.body.insertBefore(beforeEle, first);
            } else {
                if (D.getElementById("loading-redEye-bg") !== null) {
                    D.getElementById("loading-redEye-bg").outerHTML = "";
                }
            }
        }
    };
}());
/* _optimizely_evaluate=safe */