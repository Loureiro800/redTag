# redTag.js
* Helper created to facilitate a/b testing development process trough Optimizely

# Installation
* Add redTag.js library to your global Javascript

# Usage
* Optimizely Force statments to prevent flickering pages

OPTION 01 
Hide element and show them with same code by adding true or false

```javascript
/* _optimizely_evaluate=force */
redTag.hideElement('#main', true); 
/* _optimizely_evaluate=safe */

jQuery(document).ready(function($){
   /* Your code */
   redTag.hideElement('#main', false); 
});
```

OPTION 02
Hide element and show them with same code by adding true or false

```javascript
/* _optimizely_evaluate=force */
redTag.loadingBar(true); 
/* _optimizely_evaluate=safe */

jQuery(document).ready(function($){
   /* Your code */
   redTag.loadingBar(false); 
});
```